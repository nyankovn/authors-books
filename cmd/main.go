package main

import (
	"context"
	"fmt"
	"os"

	config "genius-crew-assignment/src/configuration"
	"genius-crew-assignment/src/http/rest"
	"genius-crew-assignment/src/storage/mysql"
	"genius-crew-assignment/src/util"
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	config, err := config.LoadEnv()
	if err != nil {
		return util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Environment configuration failed")
	}

	db, err := mysql.InitializeMySqlDB(config.MySQLDB)
	if err != nil {
		return util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Database connection failed: %s", err.Error())
	}

	svr := rest.NewServer(config.HTTP.Port)
	svr.Init(ctx, db)

	svr.Run()
	return nil
}
