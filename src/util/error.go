package util

import (
	"fmt"

	mssql "github.com/denisenkom/go-mssqldb"
)

const (
	ErrorCodeNotFound     = "not_found"
	ErrorCodeInvalid      = "invalid"
	ErrorCodeInternal     = "internal"
	ErrorCodeUnauthorized = "unauthorized"
	ErrorCodeUnsupported  = "unsupported"

	ErrorStatusNotFound     = 404
	ErrorStatusInvalid      = 400
	ErrorStatusInternal     = 500
	ErrorStatusUnauthorized = 403
	ErrorStatusUnsupported  = 415
)

const (
	UNIQUE_KEY_CONSTRAINT = 2627
)

// This struct represents the internal error struct.
type Error struct {
	Origin  error
	Status  int
	Code    string
	Message string
}

func (e *Error) Error() string {
	return e.Message
}

func (e *Error) ErrorMessage() string {
	return e.Message
}

func (e *Error) ErrorCode() string {
	return e.Code
}

func (e *Error) ErrorOrigin() error {
	return e.Origin
}

func WrapErrorf(origin error, status int, code string, format string, args ...interface{}) error {
	return &Error{
		Origin:  origin,
		Status:  status,
		Code:    code,
		Message: fmt.Sprintf(format, args...),
	}
}

func NewErrorf(status int, code string, format string, args ...interface{}) error {
	return WrapErrorf(nil, status, code, format, args...)
}

// UniqueViolation checks if the error is of code 23505
func UniqueViolation(err error) *mssql.Error {
	if sqlErr, ok := err.(mssql.Error); ok &&
		sqlErr.SQLErrorNumber() == UNIQUE_KEY_CONSTRAINT {
		return &sqlErr
	}
	return nil
}
