package config

import "os"

type MySQLDB struct {
	Username string
	Password string
	Host     string
	Name     string
}

func LoadMySQLDBEnv() MySQLDB {
	db := MySQLDB{
		Username: os.Getenv("MYSQL_USERNAME"),
		Password: os.Getenv("MYSQL_PASSWORD"),
		Host:     os.Getenv("MYSQL_HOST"),
		Name:     os.Getenv("DB_NAME"),
	}
	return db
}
