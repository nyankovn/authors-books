package config

import "os"

type Application struct {
	AppName string
}

func LoadApplicationEnv() Application {
	app := Application{
		AppName: os.Getenv("APP_NAME"),
	}

	return app
}
