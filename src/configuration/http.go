package config

import "os"

type HTTP struct {
	Port string
}

func LoadHTTPEnv() HTTP {
	http := HTTP{
		Port: os.Getenv("PORT"),
	}

	return http
}
