package config

import (
	"log"

	"github.com/joho/godotenv"
)

type Config struct {
	Application Application
	MySQLDB     MySQLDB
	HTTP        HTTP
}

func LoadEnv() (*Config, error) {
	err := godotenv.Load("./.env")
	if err != nil {
		log.Fatalf("Error occured while loading .env file. Err: %s", err)
		return nil, err
	}

	application := LoadApplicationEnv()
	mySQLDB := LoadMySQLDBEnv()
	http := LoadHTTPEnv()

	conf := Config{
		Application: application,
		MySQLDB:     mySQLDB,
		HTTP:        http,
	}

	return &conf, nil
}
