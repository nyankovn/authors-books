package mysql

import (
	"context"
	"database/sql"
)

type DBTX interface {
	ExecContext(context.Context, string, ...interface{}) (sql.Result, error)
	PrepareContext(context.Context, string) (*sql.Stmt, error)
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	QueryRowContext(context.Context, string, ...interface{}) *sql.Row
	QueryRow(query string, args ...interface{}) *sql.Row
}

func NewQueries(db DBTX) *Queries {
	return &Queries{DB: db}
}

type Queries struct {
	DB DBTX
}
