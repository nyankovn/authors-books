package query

const CreateBook = "INSERT INTO `book` (id, title, description, publication_date) VALUES (?, ?, ?, ?);"

const CreateAuthorBookMapping = "INSERT INTO author_book (author_id,book_id) VALUES (?, ?)"

const GetAuthorsByBookId = "SELECT a.id, a.name, a.surname FROM `author` a INNER JOIN `author_book` ab ON a.id=ab.author_id WHERE ab.book_id=?"

const GetBookByFilter = "SELECT b.id, b.title, b.description, b.publication_date FROM `book` b INNER JOIN `author_book` ab ON b.id = ab.book_id INNER JOIN `author` a ON ab.author_id=a.id WHERE b.id=? OR b.title=? OR b.description=? OR ab.author_id=?"

const UpdateBookById = ""

const DeleteBook = ""
