package query

const CreateAuthor = "INSERT INTO `author` (id, name, surname) VALUES (?, ?, ?);"

const GetAuthorsByFilter = "SELECT * FROM `author` WHERE id=? OR name=? OR surname=?"

const UpdateAuthorById = "UPDATE `author` set name=?, surname=? WHERE id=?;"

const DeleteAuthor = "DELETE FROM `author` WHERE id = ?"
