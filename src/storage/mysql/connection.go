package mysql

import (
	"context"
	"database/sql"
	"fmt"
	config "genius-crew-assignment/src/configuration"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func DSN(username string, password string, hostname string, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&loc=Local", username, password, hostname, dbName)
}

func InitializeMySqlDB(conf config.MySQLDB) (*sql.DB, error) {

	db, err := sql.Open("mysql", DSN(conf.Username, conf.Password, conf.Host, ""))
	if err != nil {
		log.Printf("Error %s when opening DB\n", err)
		return nil, err
	}
	defer db.Close()

	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	res, err := db.ExecContext(ctx, "CREATE DATABASE IF NOT EXISTS "+conf.Name)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return nil, err
	}
	no, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when fetching rows", err)
		return nil, err
	}
	log.Printf("rows affected %d\n", no)

	db.Close()
	db, err = sql.Open("mysql", DSN(conf.Username, conf.Password, conf.Host, conf.Name))
	if err != nil {
		log.Printf("Error %s when opening DB", err)
		return nil, err
	}

	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	db.SetConnMaxLifetime(time.Minute * 5)

	ctx, cancelfunc = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err = db.PingContext(ctx)
	if err != nil {
		log.Printf("Errors %s pinging DB", err)
		return nil, err
	}
	log.Printf("Connected to DB %s successfully\n", conf.Name)

	return db, nil
}
