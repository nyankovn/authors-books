package migration

const CreateBookTable = "CREATE TABLE IF NOT EXISTS book (id VARCHAR(255) PRIMARY KEY, title VARCHAR(255) NOT NULL UNIQUE, description VARCHAR(255),publication_date DATETIME DEFAULT CURRENT_TIMESTAMP);"
