package migration

const CreateAuthorTable = "CREATE TABLE IF NOT EXISTS author ( id VARCHAR(255) PRIMARY KEY, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL);"
