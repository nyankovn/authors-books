package book

import (
	"genius-crew-assignment/src/domain/author"
	"time"
)

type Book struct {
	ID              string          `json:"id" validate:"required"`
	Title           string          `json:"title" validate:"required"`
	Description     string          `json:"description"`
	Authors         []author.Author `json:"authors"`
	PublicationDate time.Time       `json:"publication_date"`
}

type CreateBookParams struct {
	Title       string          `json:"title"`
	Description string          `json:"description"`
	Authors     []author.Author `json:"authors"`
}

type UpdateBookParams struct {
	Title       string          `json:"title"`
	Description string          `json:"description"`
	Authors     []author.Author `json:"authors"`
}

type BookCollection []*Book

type AuthorBookMapping struct {
	AuthorId string `json:"author_id" validate:"required"`
	BookId   string `json:"book_id" validate:"required"`
}
