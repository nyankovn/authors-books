package book

import (
	"context"
)

type BookRepository interface {
	CreateBook(ctx context.Context, params Book) (*Book, error)
	GetBookByFilter(ctx context.Context, filter string) (BookCollection, error)
	UpdateBookById(ctx context.Context, id string, params UpdateBookParams) (*Book, error)
	DeleteBook(ctx context.Context, id string) error
}
