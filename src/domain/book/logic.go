package book

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type bookService struct {
	repository BookRepository
}

func NewBookService(repository BookRepository) BookService {
	return &bookService{
		repository: repository,
	}
}

func (s *bookService) CreateBook(ctx context.Context, params CreateBookParams) (*Book, error) {

	uuid := uuid.New().String()

	arg := Book{
		ID:              uuid,
		Title:           params.Title,
		Description:     params.Description,
		Authors:         params.Authors,
		PublicationDate: time.Now(),
	}

	author, err := s.repository.CreateBook(ctx, arg)
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (s *bookService) GetBookByFilter(ctx context.Context, filter string) (BookCollection, error) {
	books, err := s.repository.GetBookByFilter(ctx, filter)
	if err != nil {
		return nil, err
	}

	return books, nil
}

func (s *bookService) UpdateBookById(ctx context.Context, id string, params UpdateBookParams) (*Book, error) {
	return nil, nil

}

func (s *bookService) DeleteBook(ctx context.Context, id string) error {
	return nil

}
