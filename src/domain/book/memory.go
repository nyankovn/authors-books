package book

import (
	"context"
	"database/sql"
	"errors"
	"genius-crew-assignment/src/domain/author"
	repository "genius-crew-assignment/src/storage/mysql"
	"genius-crew-assignment/src/storage/mysql/migration"
	"genius-crew-assignment/src/storage/mysql/query"
	"genius-crew-assignment/src/util"

	"github.com/go-sql-driver/mysql"
)

type bookRepository struct {
	queries *repository.Queries
}

func NewBookRepository(ctx context.Context, db *sql.DB) (BookRepository, error) {
	if _, err := db.ExecContext(ctx, migration.CreateBookTable); err != nil {
		return nil, err
	}
	if _, err := db.ExecContext(ctx, migration.CreateAthorBookTable); err != nil {
		return nil, err
	}
	return &bookRepository{
		queries: repository.NewQueries(db),
	}, nil
}

func (repository *bookRepository) CreateBook(ctx context.Context, params Book) (*Book, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.CreateBook,
		&params.ID,
		&params.Title,
		&params.Description,
		&params.PublicationDate,
	)

	book := Book{
		ID:              params.ID,
		Title:           params.Title,
		Description:     params.Description,
		Authors:         params.Authors,
		PublicationDate: params.PublicationDate,
	}

	err := row.Scan(
		&book.ID,
		&book.Title,
		&book.Description,
		&book.Authors,
		&book.PublicationDate,
	)

	for _, author := range params.Authors {
		CreateAuthorBookMapping(repository, ctx, author.ID, book.ID)
	}

	if err != nil {
		var mysqlErr *mysql.MySQLError

		if err == sql.ErrNoRows {
			return &book, nil
		} else if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create book due to unique key constraint")
		} else if e := util.UniqueViolation(err); e != nil {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create book due to unique key constraint")
		}

	}
	return &book, err
}

func CreateAuthorBookMapping(repository *bookRepository, ctx context.Context, authorId string, bookId string) (*AuthorBookMapping, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.CreateAuthorBookMapping,
		authorId,
		bookId,
	)

	mapping := AuthorBookMapping{
		AuthorId: authorId,
		BookId:   bookId,
	}

	err := row.Scan(
		&mapping.AuthorId,
		&mapping.BookId,
	)

	if err != nil {
		var mysqlErr *mysql.MySQLError

		if err == sql.ErrNoRows {
			return &mapping, nil
		} else if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create author due to unique key constraint")
		} else if e := util.UniqueViolation(err); e != nil {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create author due to unique key constraint")
		}

	}
	return &mapping, err
}

func (repository *bookRepository) GetBookByFilter(ctx context.Context, filter string) (BookCollection, error) {
	rows, err := repository.queries.DB.QueryContext(ctx, query.GetBookByFilter,
		filter,
		filter,
		filter,
		filter,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	books := BookCollection{}

	for rows.Next() {
		var book Book
		if err := rows.Scan(
			&book.ID,
			&book.Title,
			&book.Description,
			&book.PublicationDate,
		); err != nil {
			return nil, err
		}
		books = append(books, &book)
	}

	for _, book := range books {

		rows, err := repository.queries.DB.QueryContext(ctx, query.GetAuthorsByBookId, book.ID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()
		var authors []author.Author
		for rows.Next() {
			var a author.Author
			if err := rows.Scan(
				&a.ID,
				&a.Name,
				&a.Surname,
			); err != nil {
				return nil, err
			}
			authors = append(authors, a)
		}
		if err := rows.Close(); err != nil {
			return nil, err
		}
		if err := rows.Err(); err != nil {
			return nil, err
		}
		book.Authors = authors
	}

	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return books, nil
}

func (repository *bookRepository) UpdateBookById(ctx context.Context, id string, params UpdateBookParams) (*Book, error) {
	return nil, nil

}

func (repository *bookRepository) DeleteBook(ctx context.Context, id string) error {
	return nil
}
