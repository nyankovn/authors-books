// Code generated by MockGen. DO NOT EDIT.
// Source: service.go

// Package mock_author is a generated GoMock package.
package mock_author

import (
	context "context"
	author "genius-crew-assignment/src/domain/author"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockAuthorService is a mock of AuthorService interface.
type MockAuthorService struct {
	ctrl     *gomock.Controller
	recorder *MockAuthorServiceMockRecorder
}

// MockAuthorServiceMockRecorder is the mock recorder for MockAuthorService.
type MockAuthorServiceMockRecorder struct {
	mock *MockAuthorService
}

// NewMockAuthorService creates a new mock instance.
func NewMockAuthorService(ctrl *gomock.Controller) *MockAuthorService {
	mock := &MockAuthorService{ctrl: ctrl}
	mock.recorder = &MockAuthorServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAuthorService) EXPECT() *MockAuthorServiceMockRecorder {
	return m.recorder
}

// CreateAuthor mocks base method.
func (m *MockAuthorService) CreateAuthor(ctx context.Context, params author.CreateAuthorParams) (*author.Author, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateAuthor", ctx, params)
	ret0, _ := ret[0].(*author.Author)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateAuthor indicates an expected call of CreateAuthor.
func (mr *MockAuthorServiceMockRecorder) CreateAuthor(ctx, params interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateAuthor", reflect.TypeOf((*MockAuthorService)(nil).CreateAuthor), ctx, params)
}

// DeleteAuthor mocks base method.
func (m *MockAuthorService) DeleteAuthor(ctx context.Context, id string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteAuthor", ctx, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteAuthor indicates an expected call of DeleteAuthor.
func (mr *MockAuthorServiceMockRecorder) DeleteAuthor(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteAuthor", reflect.TypeOf((*MockAuthorService)(nil).DeleteAuthor), ctx, id)
}

// GetAuthorsByFilter mocks base method.
func (m *MockAuthorService) GetAuthorsByFilter(ctx context.Context, filter string) (author.AuthorCollection, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAuthorsByFilter", ctx, filter)
	ret0, _ := ret[0].(author.AuthorCollection)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAuthorsByFilter indicates an expected call of GetAuthorsByFilter.
func (mr *MockAuthorServiceMockRecorder) GetAuthorsByFilter(ctx, filter interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAuthorsByFilter", reflect.TypeOf((*MockAuthorService)(nil).GetAuthorsByFilter), ctx, filter)
}

// UpdateAuthorById mocks base method.
func (m *MockAuthorService) UpdateAuthorById(ctx context.Context, id string, params author.UpdateAuthorParams) (*author.Author, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateAuthorById", ctx, id, params)
	ret0, _ := ret[0].(*author.Author)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateAuthorById indicates an expected call of UpdateAuthorById.
func (mr *MockAuthorServiceMockRecorder) UpdateAuthorById(ctx, id, params interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateAuthorById", reflect.TypeOf((*MockAuthorService)(nil).UpdateAuthorById), ctx, id, params)
}
