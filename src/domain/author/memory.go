package author

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	repository "genius-crew-assignment/src/storage/mysql"
	"genius-crew-assignment/src/storage/mysql/migration"
	"genius-crew-assignment/src/storage/mysql/query"
	"genius-crew-assignment/src/util"

	"github.com/go-sql-driver/mysql"
)

type authorRepository struct {
	queries *repository.Queries
}

func NewAuthorRepository(ctx context.Context, db *sql.DB) (AuthorRepository, error) {
	if _, err := db.ExecContext(ctx, migration.CreateAuthorTable); err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &authorRepository{
		queries: repository.NewQueries(db),
	}, nil
}

func (repository *authorRepository) CreateAuthor(ctx context.Context, params Author) (*Author, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.CreateAuthor,
		&params.ID,
		&params.Name,
		&params.Surname,
	)

	author := Author{
		ID:      params.ID,
		Name:    params.Name,
		Surname: params.Surname,
	}

	err := row.Scan(
		&author.ID,
		&author.Name,
		&author.Surname,
	)

	if err != nil {
		var mysqlErr *mysql.MySQLError

		if err == sql.ErrNoRows {
			return &author, nil
		} else if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create author due to unique key constraint")
		} else if e := util.UniqueViolation(err); e != nil {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create author due to unique key constraint")
		}

	}
	return &author, err
}

func (repository *authorRepository) GetAuthorsByFilter(ctx context.Context, filter string) (AuthorCollection, error) {
	rows, err := repository.queries.DB.QueryContext(ctx, query.GetAuthorsByFilter, filter, filter, filter)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	authors := AuthorCollection{}
	for rows.Next() {
		var author Author
		if err := rows.Scan(
			&author.ID,
			&author.Name,
			&author.Surname,
		); err != nil {
			return nil, err
		}
		authors = append(authors, &author)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return authors, nil
}

func (repository *authorRepository) UpdateAuthorById(ctx context.Context, id string, params UpdateAuthorParams) (*Author, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.UpdateAuthorById,
		&params.Name,
		&params.Surname,
		id,
	)

	author := Author{
		ID:      id,
		Name:    params.Name,
		Surname: params.Surname,
	}

	err := row.Scan(
		&author.ID,
		&author.Name,
		&author.Surname,
	)

	if err != nil {
		var mysqlErr *mysql.MySQLError

		if err == sql.ErrNoRows {
			return &author, nil
		} else if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot update author due to unique key constraint")
		} else if e := util.UniqueViolation(err); e != nil {
			return nil, util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot update author due to unique key constraint")
		}

	}
	return &author, err
}

func (repository *authorRepository) DeleteAuthor(ctx context.Context, id string) error {
	_, err := repository.queries.DB.ExecContext(ctx, query.DeleteAuthor, id)
	return err
}
