package author_test

import (
	"context"
	"database/sql"
	"fmt"
	"genius-crew-assignment/src/domain/author"
	"genius-crew-assignment/src/util"
	"log"
	"regexp"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"

	"github.com/matryer/is"
)

var a = &author.Author{
	ID:      "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
	Name:    "Nikola",
	Surname: "Yankov",
}

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestCreateAuthor(t *testing.T) {
	ctx := context.Background()
	is := is.New(t)

	db, mock := NewMock()
	defer db.Close()

	mock.ExpectExec(regexp.QuoteMeta("CREATE TABLE IF NOT EXISTS author ( id VARCHAR(255) PRIMARY KEY, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL);"))

	repo, err := author.NewAuthorRepository(ctx, db)
	if err != nil {
		fmt.Println("")
	}
	query := "INSERT INTO `author` (id, name, surname) VALUES (?, ?, ?);"

	tests := []struct {
		name      string
		arguments author.Author
		want      *sqlmock.Rows
		err       error
	}{
		{
			name: "testing creating author success",
			arguments: author.Author{
				ID:      "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
				Name:    "Nikola",
				Surname: "Yankov",
			},
			want: sqlmock.NewRows([]string{"Id", "Name", "Surname"}).AddRow(a.ID, a.Name, a.Surname),
			err:  nil,
		},
		{
			name: "testing create author failure",
			arguments: author.Author{
				ID:      "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
				Name:    "Nikola",
				Surname: "Yankov",
			},
			want: nil,
			err:  util.WrapErrorf(err, util.ErrorStatusInternal, util.ErrorCodeInternal, "Cannot create author due to unique key constraint"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			if tt.want != nil {
				mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(tt.arguments.ID, tt.arguments.Name, tt.arguments.Surname).WillReturnRows(tt.want)
				response, err := repo.CreateAuthor(ctx, tt.arguments)

				is.NoErr(err)
				is.Equal(response, a)
			} else {
				mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(tt.arguments.ID, tt.arguments.Name, tt.arguments.Surname).WillReturnError(tt.err)

				response, err := repo.CreateAuthor(ctx, tt.arguments)

				is.True(strings.Contains(err.Error(), tt.err.Error()))
				is.Equal(response, nil)
			}
		})
	}
}
