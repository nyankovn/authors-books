package author

import (
	"context"

	"github.com/google/uuid"
)

type authorService struct {
	repository AuthorRepository
}

func NewAuthorService(repository AuthorRepository) AuthorService {
	return &authorService{
		repository: repository,
	}
}

func (s *authorService) CreateAuthor(ctx context.Context, params CreateAuthorParams) (*Author, error) {
	uuid := uuid.New().String()

	arg := Author{
		ID:      uuid,
		Name:    params.Name,
		Surname: params.Surname,
	}

	author, err := s.repository.CreateAuthor(ctx, arg)
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (s *authorService) GetAuthorsByFilter(ctx context.Context, filter string) (AuthorCollection, error) {
	authors, err := s.repository.GetAuthorsByFilter(ctx, filter)
	if err != nil {
		return nil, err
	}

	return authors, nil
}

func (s *authorService) UpdateAuthorById(ctx context.Context, id string, params UpdateAuthorParams) (*Author, error) {
	arg := UpdateAuthorParams{
		Name:    params.Name,
		Surname: params.Surname,
	}

	author, err := s.repository.UpdateAuthorById(ctx, id, arg)
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (s *authorService) DeleteAuthor(ctx context.Context, id string) error {
	err := s.repository.DeleteAuthor(ctx, id)
	if err != nil {
		return err
	}
	return nil
}
