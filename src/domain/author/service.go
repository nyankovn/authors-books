package author

import "context"

//go:generate mockgen -source=service.go -destination=mock/mock_service.go -package=mock_author
type AuthorService interface {
	CreateAuthor(ctx context.Context, params CreateAuthorParams) (*Author, error)
	GetAuthorsByFilter(ctx context.Context, filter string) (AuthorCollection, error)
	UpdateAuthorById(ctx context.Context, id string, params UpdateAuthorParams) (*Author, error)
	DeleteAuthor(ctx context.Context, id string) error
}
