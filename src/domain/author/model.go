package author

type Author struct {
	ID      string `json:"id" validate:"required"`
	Name    string `json:"name" validate:"required"`
	Surname string `json:"surname" validate:"required"`
	// Books   []book.Book `json:"books"`
}

type CreateAuthorParams struct {
	Name    string `json:"name" validate:"required"`
	Surname string `json:"surname" validate:"required"`
	// Books   []book.Book `json:"books"`
}

type UpdateAuthorParams struct {
	Name    string `json:"name"`
	Surname string `json:"surname"`
	// Books   []book.Book `json:"books"`
}

type AuthorCollection []*Author
