package author

import (
	"context"
)

//go:generate mockgen -source=repository.go -destination=mock/mock_repository.go -package=mock_author
type AuthorRepository interface {
	CreateAuthor(ctx context.Context, params Author) (*Author, error)
	GetAuthorsByFilter(ctx context.Context, filter string) (AuthorCollection, error)
	UpdateAuthorById(ctx context.Context, id string, params UpdateAuthorParams) (*Author, error)
	DeleteAuthor(ctx context.Context, id string) error
}
