package rest

import (
	"encoding/json"
	"genius-crew-assignment/src/domain/book"
	"genius-crew-assignment/src/util"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

func (s *server) CreateBook(w http.ResponseWriter, r *http.Request) {
	var request book.CreateBookParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	book, err := s.BookService.CreateBook(r.Context(), request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, book)
}

func (s *server) GetBookByFilter(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	book, err := s.BookService.GetBookByFilter(r.Context(), vars["filter"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, book)
}

func (s *server) UpdateBookById(w http.ResponseWriter, r *http.Request) {

}

func (s *server) DeleteBook(w http.ResponseWriter, r *http.Request) {

}
