package rest

import (
	"encoding/json"
	"genius-crew-assignment/src/domain/author"
	"genius-crew-assignment/src/util"

	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

func (s *server) CreateAuthor(w http.ResponseWriter, r *http.Request) {
	var request author.CreateAuthorParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	author, err := s.AuthorService.CreateAuthor(r.Context(), request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, author)
}

func (s *server) GetAuthorByFilter(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	author, err := s.AuthorService.GetAuthorsByFilter(r.Context(), vars["filter"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, author)
}

func (s *server) UpdateAuthorById(w http.ResponseWriter, r *http.Request) {
	var request author.UpdateAuthorParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	vars := mux.Vars(r)

	author, err := s.AuthorService.UpdateAuthorById(r.Context(), vars["id"], request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, author)
}

func (s *server) DeleteAuthor(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := s.AuthorService.DeleteAuthor(r.Context(), vars["id"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, "Author is deleted")
}
