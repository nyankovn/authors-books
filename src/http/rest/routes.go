package rest

func (s *server) routes() {

	authors := s.Router.PathPrefix("/authors").Subrouter()
	books := s.Router.PathPrefix("/books").Subrouter()

	authors.HandleFunc("", s.CreateAuthor).Methods("POST")
	authors.HandleFunc("/{filter}", s.GetAuthorByFilter).Methods("GET")
	authors.HandleFunc("/{id}", s.UpdateAuthorById).Methods("PUT")
	authors.HandleFunc("/{id}", s.DeleteAuthor).Methods("DELETE")

	books.HandleFunc("", s.CreateBook).Methods("POST")
	books.HandleFunc("/{filter}", s.GetBookByFilter).Methods("GET")
}
