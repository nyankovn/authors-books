package rest

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"genius-crew-assignment/src/domain/author"
	"genius-crew-assignment/src/domain/book"
	"genius-crew-assignment/src/util"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

type server struct {
	Server    *http.Server
	Router    *mux.Router
	Validator *validator.Validate

	BookService   book.BookService
	AuthorService author.AuthorService
}

func NewServer(port string) *server {
	address := ":" + port

	svr := &server{
		Server: &http.Server{
			Addr:         address,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
		Router: mux.NewRouter(),
	}

	svr.Router.NotFoundHandler = http.HandlerFunc(handleNotFound)

	svr.routes()

	return svr
}

func (s *server) Init(ctx context.Context, db *sql.DB) {
	s.Validator = validator.New()

	bookRepository, err := book.NewBookRepository(ctx, db)
	if err != nil {
		fmt.Println("Book repo can not be initialized")
	}

	authorRepository, err := author.NewAuthorRepository(ctx, db)
	if err != nil {
		fmt.Println("Author repo can not be initialized")
	}

	s.BookService = book.NewBookService(bookRepository)
	s.AuthorService = author.NewAuthorService(authorRepository)
}

func (s *server) Run() {

	s.Server.Handler = s.Router

	if err := s.Server.ListenAndServe(); err != nil {
		log.Println(err)
	}

}

func handleNotFound(w http.ResponseWriter, r *http.Request) {
	err := util.NewErrorf(util.ErrorStatusNotFound, util.ErrorCodeNotFound, "Endpoint was not found.")
	renderErrorResponse(r.Context(), w, util.ErrorStatusNotFound, util.ErrorCodeNotFound, err.Error())
}
